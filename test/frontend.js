let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../server/server.js');
let should = chai.should();


chai.use(chaiHttp);

describe('Frontend: index.html', () => {
  it('it should GET index.html', (done) => {
    chai.request(server)
      .get('/')
      .end((err, res) => {
        res.should.have.status(200);
        res.should.be.html;
        done();
      });
  });
});


